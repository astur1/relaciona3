﻿DROP DATABASE IF EXISTS tiendamulti;
CREATE DATABASE IF NOT EXISTS tiendamulti;
USE tiendamulti;
CREATE TABLE productos (
idp int,
  peso int,
  nombrep varchar(40),
  PRIMARY KEY(idp)
);
CREATE TABLE tienda(
codigot int,
  direccion varchar(40),
  PRIMARY KEY(codigot)
);
CREATE TABLE clientes(
idc int,
  nombrec varchar(40),
  PRIMARY KEY(idc)
);
CREATE TABLE telefono(
idc int,
  idp int,
  codigot int,
  PRIMARY KEY(idc, idp, codigot),
  CONSTRAINT telecli FOREIGN KEY(idc) REFERENCES clientes(idc),
  CONSTRAINT teletien FOREIGN KEY(codigot) REFERENCES tienda(codigot),
  CONSTRAINT telepro FOREIGN KEY(idp) REFERENCES productos(idp)
);
CREATE TABLE compran(
fecha date,
  cantidad int,
  direccion varchar(40),
  codigot int,
  idp int,
  idc int,
  PRIMARY KEY(idp, idc, codigot),
  UNIQUE KEY(fecha, idp, codigot),
  CONSTRAINT comproductos FOREIGN KEY(idp) REFERENCES productos (idp),
  CONSTRAINT comclientes FOREIGN KEY (idc) REFERENCES clientes (idc),
  CONSTRAINT comtiend FOREIGN KEY(codigot) REFERENCES tienda (codigot)
);